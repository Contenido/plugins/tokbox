package tokbox::Session;

use strict;
use warnings 'all';
use Contenido::Globals;
use tokbox::Keeper;

our $P2P_PREFERENCE = 'p2p.preference';

sub new {
    my ($proto, %params) = @_;
    my $class = ref($proto) || $proto;
    my $self = {};
    bless $self, $class;

    $self->{sessionId} = $params{sessionId} || '';
    $self->{sessionProperties} = $params{properties};

    return $self;
}


sub sessionId {
    my $self = shift;
    return exists $self->{sessionId} && $self->{sessionId} ? $self->{sessionId} : undef;
}

sub sessionProperties {
    my $self = shift;
    return exists $self->{sessionProperties} && $self->{sessionProperties} ? $self->{sessionProperties} : undef;
}

sub getSessionId {
    my $self = shift;
    return $self->sessionId;
}

sub id {
    my $self = shift;
    return $self->sessionId;
}

1;
