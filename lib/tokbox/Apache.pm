package tokbox::Apache;

use strict;
use warnings 'all';

use tokbox::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{tokbox} = tokbox::Keeper->new($state->tokbox);
}

sub request_init {
}

sub child_exit {
}

1;
