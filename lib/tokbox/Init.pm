package tokbox::Init;

use strict;
use warnings 'all';

use Contenido::Globals;
use tokbox::Apache;
use tokbox::Keeper;
use tokbox::Session;


# загрузка всех необходимых плагину классов
# tokbox::SQL::SomeTable
# tokbox::SomeClass
Contenido::Init::load_classes(qw(
	));

sub init {
	0;
}

1;
